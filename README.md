# Qt System Monitor

Qt System Monitor (qsysmon) allows you to watch CPU temperature, CPU usage and free space on the root partition.
It is composed by two applications communicating via MQTT, the `probe` daemon and the `data-display` client that allows to view the system parameters and change the sampling frequency.

It has been developed using CLion (instead of the buggy Qt Creator), hence it is a CMake-based project.
Developed and tested on Ubuntu 18.10, using the Qt (=>5.10) framework.

## Build from sources

### Install dependencies
Install CMake, Qt5 and Mosquitto:

    sudo apt-get install build-essential cmake qt5-default mosquitto

Then install the Qt module qmqtt:

    cd <somewhere>
    git clone https://github.com/emqtt/qmqtt.git
    cd qmqtt
    mkdir build
    cd build
    qmake -r ..
    make
    sudo make install
    #qmqtt CMake module is buggy, use this quick workaround
    sudo ln -s /usr/lib/x86_64-linux-gnu/libqmqtt.so.1.0.0 /usr/lib/x86_64-linux-gnu/libQt5qmqtt.so.1.0.0

### Configure the MQTT broker (optional)
You may want to configure the MQTT broker, for example to setup authentication.
Use [any tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-the-mosquitto-mqtt-messaging-broker-on-ubuntu-16-04) available online to do that.

### Build
Build the project with the standard CMake procedure:

    mkdir build
    cd build
    cmake ..
    make

## Run probe
To run the probe daemon:

    ./build/probe/probe [configuration file path]

If not specified, the default configuration file path is `~/.qtassignment-probe.json`.
The configuration file format is described in `CONFIG.md`.

*Note*: if an invalid configuration file is specified, or if the default path cannot be used, changes to the `period` configuration will be *discarded* when the `probe` process terminates.

## Run data display
To run the data display client:

    ./build/data-display/data-display [configuration file path]

If not specified, the default configuration file path is `~/.qtassignment-data-display.json`.
The configuration file format is described in `CONFIG.md`.

