/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <QCoreApplication>
#include <QTextStream>
#include <QtCore/QDir>
#include "DataDisplayConfig.h"
#include "Display.h"
#include "Input.h"
#include "MqttClient.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    Input input;
    Display display;

    QString configurationFile = QDir::homePath() + "/.qtassignment-data-display.json";
    if (argc > 1) {
        configurationFile = app.arguments().at(1);
    }
    DataDisplayConfig *config = new DataDisplayConfig();
    config->readFile(configurationFile);
    MqttClient client(config);

    QObject::connect(&display, &Display::samplingIntervalChanged, &client, &MqttClient::onSamplingIntervalChanged);

    QObject::connect(&client, &MqttClient::cpuTemperatureChanged, &display, &Display::onCpuTemperatureChanged);
    QObject::connect(&client, &MqttClient::cpuUsageChanged, &display, &Display::onCpuUsageChanged);
    QObject::connect(&client, &MqttClient::diskFreeChanged, &display, &Display::onDiskFreeChanged);

    QObject::connect(&input, &Input::inputStarted, &display, &Display::onInputStarted);
    QObject::connect(&input, &Input::inputCompleted, &display, &Display::onInputCompleted);

    client.connectToHost();
    input.start();
    display.start();

    return app.exec();
}
