/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#pragma once

#include <qmqtt>
#include "../common/topics.h"
#include "DataDisplayConfig.h"

class MqttClient : public QMQTT::Client {
Q_OBJECT

public:
    MqttClient(DataDisplayConfig *config);

private:
    quint16 msgId = 0;

    void readConfig(DataDisplayConfig *config);

public slots:
    void onConnected();
    void onError(QMQTT::ClientError error);
    void onReceived(const QMQTT::Message& message);

    void onSamplingIntervalChanged(int T);

signals:
    void cpuTemperatureChanged(float value);
    void cpuUsageChanged(float value);
    void diskFreeChanged(qint64 value);
};
