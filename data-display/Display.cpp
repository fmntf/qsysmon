/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "Display.h"

void Display::onInputStarted()
{
    this->isTyping = true;
}

void Display::onInputCompleted(const QString &command)
{
    this->isTyping = false;
    QString input = command.toUpper();

    if (input.length() == 0) return;

    if (input == "T") {
        displayProperty(DisplayProperty::CpuTemperature);
    } else if (input == "U") {
        displayProperty(DisplayProperty::CpuUsage);
    } else if (input == "F") {
        displayProperty(DisplayProperty::DiskFree);
    } else if (input == "S") {
        stopDisplay();
    } else {
        bool isInteger;
        int T = input.toInt(&isInteger);
        if (isInteger && T >= 500 && T <= 5000) {
            setPeriod(T);
        } else {
            QTextStream(stdout) << "Invalid command!" << endl;
        }
    }
}

void Display::start()
{
    QTextStream(stdout) <<
        "Type:" << endl <<
        "T to start displaying CPU temperature" << endl <<
        "U to start displaying CPU usage" << endl <<
        "F to start displaying free space of the root partition" << endl <<
        "S to stop displaying" << endl <<
        "a number from 500 to 5000 to set the probing interval" << endl;
}

void Display::displayProperty(DisplayProperty newProperty)
{
    if (this->displaying == newProperty) {
        return; // if the application is already printing that type of property, nothing happens
    }

    if (newProperty == DisplayProperty::CpuTemperature) {
        QTextStream(stdout) << "Start display of CPU temperature..." << endl;
    }

    if (newProperty == DisplayProperty::CpuUsage) {
        QTextStream(stdout) << "Start display of CPU usage..." << endl;
    }

    if (newProperty == DisplayProperty::DiskFree) {
        QTextStream(stdout) << "Start display of free space of the root partition..." << endl;
    }

    this->displaying = newProperty;
}

void Display::stopDisplay()
{
    if (this->displaying == DisplayProperty::None) {
        return; // if the application is already stopped, nothing happens
    }

    QTextStream(stdout) << "Stop display." << endl;
    this->displaying = DisplayProperty::None;
}

void Display::onCpuTemperatureChanged(float value)
{
    if (!this->isTyping && this->displaying == DisplayProperty::CpuTemperature) {
        printf("%6.2f °C\n", value);
    }
}

void Display::onCpuUsageChanged(float value)
{
    if (!this->isTyping && this->displaying == DisplayProperty::CpuUsage) {
        printf("%6.2f %%\n", value);
    }
}

void Display::onDiskFreeChanged(qint64 value)
{
    if (!this->isTyping && this->displaying == DisplayProperty::DiskFree) {
        QTextStream(stdout) << this->locale.formattedDataSize(value, 2, QLocale::DataSizeTraditionalFormat) << endl;
    }
}

void Display::setPeriod(int T)
{
    QTextStream(stdout) << "Set of probe sampling interval to " << T << " ms." << endl;
    emit samplingIntervalChanged(T);
}
