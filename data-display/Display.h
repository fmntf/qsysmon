/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#pragma once

#include <iostream>
#include <QTextStream>
#include <QThread>
#include "MqttClient.h"
#include "DisplayProperty.h"

class Display : public QObject {
Q_OBJECT
public:
    void start();


private:
    DisplayProperty displaying = DisplayProperty::None;

    void displayProperty(DisplayProperty newProperty);
    void stopDisplay();
    void setPeriod(int T);

    bool isTyping = false;
    QLocale locale;

public slots:
    void onCpuTemperatureChanged(float value);
    void onCpuUsageChanged(float value);
    void onDiskFreeChanged(qint64 value);

    void onInputStarted();
    void onInputCompleted(const QString &command);

signals:
    void samplingIntervalChanged(int T);
};
