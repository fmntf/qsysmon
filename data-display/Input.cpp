/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "Input.h"

struct termios originalSettings;
void disableRawMode() {
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &originalSettings);
}

QThread *inputThread;
void onSignal(int sig) {
    disableRawMode();
    inputThread->requestInterruption();
}

void Input::run()
{
    this->enableRawMode();
    inputThread = QThread::currentThread();

    signal(SIGINT, onSignal);
    signal(SIGTERM, onSignal);

    char c;
    while (!QThread::currentThread()->isInterruptionRequested()) {
        c = 0;
        read(STDIN_FILENO, &c, 1);
        if (c == 0) continue;

        if (!this->isUserTyping) {
            this->isUserTyping = true;
            emit inputStarted();
        }

        if (c == 10) {
            if (this->isUserTyping) {
                this->isUserTyping = false;
                emit inputCompleted(input.trimmed());
                input = "";
            }
        } else {
            input.append(c);
        }
    }

    QTimer::singleShot(0, qApp, &QCoreApplication::quit);
}

void Input::enableRawMode() {
    tcgetattr(STDIN_FILENO, &originalSettings);
    atexit(disableRawMode);

    struct termios newSettings = originalSettings;
    newSettings.c_lflag &= ~ICANON;
    newSettings.c_cc[VMIN] = 0;
    newSettings.c_cc[VTIME] = 1;

    tcsetattr(STDIN_FILENO, TCSAFLUSH, &newSettings);
}
