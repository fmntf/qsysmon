/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "MqttClient.h"

MqttClient::MqttClient(DataDisplayConfig *config) : QMQTT::Client()
{
    this->readConfig(config);
    this->setAutoReconnect(true);

    connect(this, &MqttClient::connected, this, &MqttClient::onConnected);
    connect(this, &MqttClient::error, this, &MqttClient::onError);
    connect(this, &MqttClient::received, this, &MqttClient::onReceived);
}

void MqttClient::onConnected()
{
    subscribe(TOPIC_CPU_TEMPERATURE, 0);
    subscribe(TOPIC_CPU_USAGE, 0);
    subscribe(TOPIC_DISK_FREE_SPACE, 0);
}

void MqttClient::onError(const QMQTT::ClientError error)
{
    if (error == QMQTT::ClientError::MqttServerUnavailableError ||
        error == QMQTT::ClientError::SocketConnectionRefusedError ||
        error == QMQTT::ClientError::SocketHostNotFoundError) {
        QTextStream(stderr) << "Cannot connect to MQTT: server unavailable" << endl;
        return;
    }

    if (error == QMQTT::ClientError::MqttBadUserNameOrPasswordError ||
        error == QMQTT::ClientError::MqttNotAuthorizedError) {
        QTextStream(stderr) << "Cannot connect to MQTT: unauthorized" << endl;
        return;
    }

    if (error == QMQTT::ClientError::SocketRemoteHostClosedError) {
        QTextStream(stderr) << "Cannot connect to MQTT: connection closed by remote host" << endl;
        return;
    }

    QTextStream(stderr) << "Cannot connect to MQTT: error #" << error << endl;
}

void MqttClient::onReceived(const QMQTT::Message &message)
{
    QString payload = QString::fromUtf8(message.payload());
    QString topic = message.topic();

    if (topic == TOPIC_CPU_TEMPERATURE) {
        emit cpuTemperatureChanged(payload.toFloat());
    } else if (topic == TOPIC_CPU_USAGE) {
        emit cpuUsageChanged(payload.toFloat());
    } else if (topic == TOPIC_DISK_FREE_SPACE) {
        emit diskFreeChanged(payload.toULongLong());
    }
}

void MqttClient::onSamplingIntervalChanged(int T)
{
    this->msgId++; // we don't care if it automatically overflows
    publish(QMQTT::Message(this->msgId, TOPIC_SET_PERIOD, QString::number(T).toUtf8()));
}

void MqttClient::readConfig(DataDisplayConfig *config)
{
    this->setHostName(config->getMqttHost());
    this->setPort(config->getMqttPort());

    if (config->getMqttClientId() != nullptr) {
        this->setClientId(config->getMqttClientId());
    }

    if (config->getMqttUsername() != nullptr) {
        this->setUsername(config->getMqttUsername());
    }

    if (config->getMqttPassword() != nullptr) {
        this->setPassword(config->getMqttPassword().toUtf8());
    }
}
