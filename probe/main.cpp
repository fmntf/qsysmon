/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <QCoreApplication>
#include <QTextStream>
#include <QtCore/QDir>
#include "MqttClient.h"
#include "ProbeConfig.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    QString configurationFile = QDir::homePath() + "/.qtassignment-probe.json";
    if (argc > 1) {
        configurationFile = app.arguments().at(1);
    }
    ProbeConfig *config = new ProbeConfig();
    config->readFile(configurationFile);

    MqttClient *client = new MqttClient(config);
    QTextStream(stdout) << "Connecting to MQTT broker... ";
    client->connectToHost();

    return app.exec();
}
