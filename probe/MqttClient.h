/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#pragma once

#include <QCoreApplication>
#include <QTimer>
#include <qmqtt.h>
#include <monitors/Cpu.h>
#include <monitors/Disk.h>
#include "../common/topics.h"
#include "ProbeConfig.h"

class MqttClient : public QMQTT::Client {
Q_OBJECT

public:
    MqttClient(ProbeConfig *config);

private:
    QTimer *timer;
    ProbeConfig *config;
    Cpu *cpu;
    Disk *disk;
    int period = 1000;
    quint16 msgId = 0;

    void publishMessage(const QString &topic, const QString &payload);
    void readConfig(ProbeConfig *config);

public slots:
    void onConnected();
    void onTimeout();
    void onDisconnected();
    void onError(QMQTT::ClientError error);
    void onReceived(const QMQTT::Message& message);
};
