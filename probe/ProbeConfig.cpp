/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "ProbeConfig.h"

void ProbeConfig::initOptions(QJsonObject map)
{
    if (map.contains(CONFIG_PROBE_PERIOD)) {
        int period = map[CONFIG_PROBE_PERIOD].toInt();
        if (period >= 500 && period <= 5000) {
            this->period = period;
        } else {
            QTextStream(stderr) << "Invalid period value, using default value." << endl;
        }
    }
}

int ProbeConfig::getPeriod()
{
    return this->period;
}

void ProbeConfig::updatePeriod(int T)
{
    if (this->writableFile == nullptr) {
        return; // we could not open/read the configuration file, won't risk to write on it
    }

    QFile file(this->writableFile);
    bool opened = file.open(QIODevice::WriteOnly | QIODevice::Text);
    if (!opened) {
        QTextStream(stderr) << "Cannot open configuration file: " << this->writableFile << endl;
        return;
    }

    QJsonObject set;
    set[CONFIG_PROBE_MQTT_HOST] = this->mqttHost;
    set[CONFIG_PROBE_MQTT_PORT] = this->mqttPort;
    set[CONFIG_PROBE_MQTT_CLIENT_ID] = this->mqttClientId;

    if (this->mqttUsername != nullptr) {
        set[CONFIG_PROBE_MQTT_USERNAME] = this->mqttUsername;
    }
    if (this->mqttPassword != nullptr) {
        set[CONFIG_PROBE_MQTT_PASSWORD] = this->mqttPassword;
    }

    set[CONFIG_PROBE_PERIOD] = T;

    QJsonDocument document(set);
    qint64 written = file.write(document.toJson());
    if (written < 0) {
        QTextStream(stderr) << "Could not write configuration file: " << this->writableFile << endl;
    }
}
