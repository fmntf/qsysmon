/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "MqttClient.h"

MqttClient::MqttClient(ProbeConfig *config) : QMQTT::Client()
{
    this->readConfig(config);
    this->setAutoReconnect(true);

    connect(this, &MqttClient::connected, this, &MqttClient::onConnected);
    connect(this, &MqttClient::disconnected, this, &MqttClient::onDisconnected);
    connect(this, &MqttClient::error, this, &MqttClient::onError);
    connect(this, &MqttClient::received, this, &MqttClient::onReceived);

    this->cpu = new Cpu();
    this->disk = new Disk();
}

void MqttClient::onConnected()
{
    QTextStream(stdout) << "Connection to MQTT broker established!" << endl;
    subscribe(TOPIC_SET_PERIOD, 0);

    this->msgId = 0;
    this->timer = new QTimer(this);
    this->timer->start(this->period);
    connect(this->timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
}

void MqttClient::onDisconnected()
{
    QTextStream(stdout) << "Connection to MQTT broker lost!" << endl;

    this->timer->stop();
    disconnect(this->timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
}

void MqttClient::onError(const QMQTT::ClientError error)
{
    if (error == QMQTT::ClientError::MqttServerUnavailableError ||
        error == QMQTT::ClientError::SocketConnectionRefusedError ||
        error == QMQTT::ClientError::SocketHostNotFoundError) {
        QTextStream(stderr) << "Cannot connect to MQTT: server unavailable" << endl;
        return;
    }

    if (error == QMQTT::ClientError::MqttBadUserNameOrPasswordError ||
        error == QMQTT::ClientError::MqttNotAuthorizedError) {
        QTextStream(stderr) << "Cannot connect to MQTT: unauthorized" << endl;
        return;
    }

    if (error == QMQTT::ClientError::SocketRemoteHostClosedError) {
        QTextStream(stderr) << "Cannot connect to MQTT: connection closed by remote host" << endl;
        return;
    }

    QTextStream(stderr) << "Cannot connect to MQTT: error #" << error << endl;
}

void MqttClient::onReceived(const QMQTT::Message &message)
{
    QString payload = QString::fromUtf8(message.payload());

    if (message.topic() == TOPIC_SET_PERIOD) {
        int T = payload.toInt();
        QTextStream(stdout) << "Sampling data every " << T << "ms." << endl;
        this->period = T;
        this->timer->setInterval(T);
        this->config->updatePeriod(T);
    }
}

void MqttClient::onTimeout()
{
    float cpuTemp = this->cpu->getTemperature();
    if (cpuTemp >= 0) {
        publishMessage(TOPIC_CPU_TEMPERATURE, QString::number(cpuTemp));
    } else {
        QTextStream(stdout) << "Invalid CPU temperature: " << cpuTemp << endl;
    }

    float cpuUsage = this->cpu->getUsage();
    if (cpuUsage >= 0) {
        publishMessage(TOPIC_CPU_USAGE, QString::number(cpuUsage));
    } else {
        QTextStream(stdout) << "Invalid CPU usage: " << cpuUsage << endl;
    }

    qint64 diskFree = this->disk->getFreeSpace();
    if (diskFree >= 0) {
        publishMessage(TOPIC_DISK_FREE_SPACE, QString::number(diskFree));
    } else {
        QTextStream(stdout) << "Invalid disk free: " << diskFree << endl;
    }
}

void MqttClient::publishMessage(const QString &topic, const QString &payload)
{
    this->msgId++; // we don't care if it automatically overflows
    publish(QMQTT::Message(this->msgId, topic, payload.toUtf8()));
}

void MqttClient::readConfig(ProbeConfig *config)
{
    this->config = config;

    this->setHostName(config->getMqttHost());
    this->setPort(config->getMqttPort());

    if (config->getMqttClientId() != nullptr) {
        this->setClientId(config->getMqttClientId());
    }

    if (config->getMqttUsername() != nullptr) {
        this->setUsername(config->getMqttUsername());
    }

    if (config->getMqttPassword() != nullptr) {
        this->setPassword(config->getMqttPassword().toUtf8());
    }

    this->period = config->getPeriod();
}
