/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "Cpu.h"

Cpu::Cpu() {
    this->timer = new QTimer(this);
    this->timer->start(400);
    connect(this->timer, SIGNAL(timeout()), this, SLOT(onTimeout()));

    readCpuStats(cpuStats0);
}

float Cpu::getTemperature() {
    QFile f("/sys/class/thermal/thermal_zone0/temp");
    if (!f.open(QFile::ReadOnly | QFile::Text)) return -1;
    QTextStream in(&f);

    return in.readAll().toFloat() / (float)1000.0;
}

float Cpu::getUsage() {
    return this->cpuUsage;
}

void Cpu::readCpuStats(long double cpuStats[4])
{
    FILE *fp;
    fp = fopen("/proc/stat","r");
    if (fp == nullptr) return;
    fscanf(fp,"%*s %Lf %Lf %Lf %Lf", &cpuStats[0], &cpuStats[1], &cpuStats[2], &cpuStats[3]);
    fclose(fp);
}

/**
 * https://stackoverflow.com/a/3780874
 */
void Cpu::onTimeout() {
    readCpuStats(cpuStats1);
    if (cpuStats1[0] == 0 && cpuStats1[1] == 0 && cpuStats1[2] == 0 && cpuStats1[3] == 0) {
        this->cpuUsage = -1;
        return;
    }

    long double busy0, busy1, idle0, idle1;

    busy0 = cpuStats0[0] + cpuStats0[1] + cpuStats0[2];
    busy1 = cpuStats1[0] + cpuStats1[1] + cpuStats1[2];
    idle0 = cpuStats0[3];
    idle1 = cpuStats1[3];

    this->cpuUsage = (busy1-busy0) / ( (busy1+idle1) - (busy0+idle0) ) * (float)100.0;

    cpuStats0[0] = cpuStats1[0];
    cpuStats0[1] = cpuStats1[1];
    cpuStats0[2] = cpuStats1[2];
    cpuStats0[3] = cpuStats1[3];
}