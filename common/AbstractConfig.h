/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#pragma once

#include <QtCore/QString>
#include <QtCore/QFileInfo>
#include <QtCore/QTextStream>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>

class AbstractConfig {
public:
    const QString CONFIG_PROBE_MQTT_HOST      = "mqtt_host";
    const QString CONFIG_PROBE_MQTT_PORT      = "mqtt_port";
    const QString CONFIG_PROBE_MQTT_CLIENT_ID = "mqtt_client_id";
    const QString CONFIG_PROBE_MQTT_USERNAME  = "mqtt_username";
    const QString CONFIG_PROBE_MQTT_PASSWORD  = "mqtt_password";

    QString getMqttHost();
    quint16 getMqttPort();
    QString getMqttClientId();
    QString getMqttUsername();
    QString getMqttPassword();

    void readFile(const QString &filePath);

protected:
    QString writableFile = nullptr;
    QString mqttHost     = "localhost";
    quint16 mqttPort     = 1883;
    QString mqttClientId = nullptr;
    QString mqttUsername = nullptr;
    QString mqttPassword = nullptr;

    virtual void initOptions(QJsonObject map) {};
};
