/**
 * Qt System Monitor
 * Copyright (C) 2019 Francesco Montefoschi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "AbstractConfig.h"

void AbstractConfig::readFile(const QString &filePath)
{
    QFileInfo check_file(filePath);
    if (!check_file.exists()) {
        QTextStream(stderr) << "Missing configuration file: " << filePath
                            << ", using default settings." << endl;
        this->writableFile = filePath;
        return;
    }

    if (!check_file.isFile()) {
        QTextStream(stderr) << "Configuration file is not a file: " << filePath
                            << ", using default settings." << endl;
        return;
    }

    QString json;
    QFile file(filePath);
    bool opened = file.open(QIODevice::ReadOnly | QIODevice::Text);
    if (!opened) {
        QTextStream(stderr) << "Cannot open configuration file: " << filePath
                            << ", using default settings." << endl;
        return;
    }
    QJsonDocument document = QJsonDocument::fromJson(QString(file.readAll()).toUtf8());
    file.close();

    if (!document.isObject()) {
        QTextStream(stderr) << "Malformed configuration file: " << filePath
                            << ", using default settings." << endl;
        return;
    }

    this->writableFile = filePath;
    QJsonObject map = document.object();

    if (map.contains(CONFIG_PROBE_MQTT_HOST)) {
        this->mqttHost = map[CONFIG_PROBE_MQTT_HOST].toString();
    }
    if (map.contains(CONFIG_PROBE_MQTT_PORT)) {
        int mqttPort = map[CONFIG_PROBE_MQTT_PORT].toInt();
        if (mqttPort > 0 && mqttPort <= 65536) {
            this->mqttPort = (quint16) mqttPort;
        } else {
            QTextStream(stderr) << "Invalid MQTT port, using default value." << endl;
        }
    }
    if (map.contains(CONFIG_PROBE_MQTT_CLIENT_ID)) {
        this->mqttClientId = map[CONFIG_PROBE_MQTT_CLIENT_ID].toString();
    }
    if (map.contains(CONFIG_PROBE_MQTT_USERNAME)) {
        this->mqttUsername = map[CONFIG_PROBE_MQTT_USERNAME].toString();
    }
    if (map.contains(CONFIG_PROBE_MQTT_PASSWORD)) {
        this->mqttPassword = map[CONFIG_PROBE_MQTT_PASSWORD].toString();
    }

    this->initOptions(map);
}

QString AbstractConfig::getMqttHost()
{
    return this->mqttHost;
}

quint16 AbstractConfig::getMqttPort()
{
    return this->mqttPort;
}

QString AbstractConfig::getMqttClientId()
{
    return this->mqttClientId;
}

QString AbstractConfig::getMqttUsername()
{
    return this->mqttUsername;
}

QString AbstractConfig::getMqttPassword()
{
    return this->mqttPassword;
}
