# Qt System Monitor Configuration

## Probe
The configuration file should look like this:
```
{
    "mqtt_client_id": "probe",
    "mqtt_host": "localhost",
    "mqtt_port": 1883,
    "mqtt_username": "username",
    "mqtt_password": "password",
    "period": 1000
}
```
where:
 * `mqtt_client_id` specifies the MQTT client ID, defaults to `probe`;
 * `mqtt_host` specifies the MQTT broker hostname, defaults to `localhost`;
 * `mqtt_port` specifies the MQTT broker port, defaults to `1883`;
 * `mqtt_username` specifies the MQTT username (omit it to disable the authentication);
 * `mqtt_password` specifies the MQTT password (omit it to disable the authentication);
 * `period` specifies the sampling period in milliseconds, defaults to `1000`.

Bear in mind that the file must be writable in order to let the `probe` application to persist the `period` value.

## Data Display
The configuration file is like the one used by `probe`, except that there is no `period` parameter.

The file may also be read only, since it is never updated.
